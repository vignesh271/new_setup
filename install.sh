#!/bin/sh
apt-get update  # To get the latest package lists
apt-get install autoconf automake bison build-essential curl git-core libapr1 libaprutil1 libc6-dev libltdl-dev libreadline6 libreadline6-dev libsqlite3-0 libsqlite3-dev libssl-dev libtool libxml2-dev libxslt-dev libxslt1-dev libyaml-dev ncurses-dev nodejs openssl sqlite3 zlib1g zlib1g-dev -y
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | tee /etc/apt/sources.list.d/sublime-text.list
apt-get update
apt-get install -y sublime-text
apt-get install -y curl gnupg build-essential
apt-add-repository -y ppa:rael-gc/rvm
apt-get update
apt-get install rvm -y
rvm install ruby
apt-get update
apt-get install -y nodejs &&
ln -sf /usr/bin/nodejs /usr/local/bin/node
gem install bundler --no-rdoc --no-ri
apt-get install bc bison build-essential ccache curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libssl-dev libwxgtk3.0-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc zip zlib1g-dev -y
wget -q https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip platform-tools-latest-linux.zip -d ~
echo "# add Android SDK platform tools to path" >> ~/.profile
echo "if [ -d "$HOME/platform-tools" ] ; then" >> ~/.profile
echo "    PATH="$HOME/platform-tools:$PATH"" >> ~/.profile
echo "fi" >> ~/.profile
source ~/.profile
apt-get update
apt-get install -y openjdk-8-jdk
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
echo "# set PATH so it includes user's private bin if it exists" >> ~/.profile
echo "if [ -d "$HOME/bin" ] ; then" >> ~/.profile
echo "    PATH="$HOME/bin:$PATH"" >> ~/.profile
echo "fi" >> ~/.profile
source ~/.profile
apt-get update
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
pip install selenium
wget https://www.scootersoftware.com/bcompare-4.2.5.23088_amd64.deb
apt-get update
apt-get install gdebi-core
gdebi bcompare-4.2.5.23088_amd64.deb
#etc.